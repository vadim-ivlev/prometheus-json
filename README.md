# prometheus-json

https://github.com/prometheus-community/json_exporter

https://levelup.gitconnected.com/transforming-remote-json-into-prometheus-metrics-334d772df38a

https://docs.docker.com/config/daemon/prometheus/

# Runnig
```
# python3 -m http.server 8000
sh/up-front.sh
sh/up.sh

```
then Prometheus is available at

http://localhost:9090/graph

htp://localhost:3000


## urls

http://localhost:9090/graph?g0.expr=example_global_value&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h


```
docker run --rm \
                -p 7979:7979 \
                    -v ./config.yml:/config.yml \
                --add-host=host.docker.internal:host-gateway \
                quay.io/prometheuscommunity/json-exporter \
                --config.file=/config.yml

```

```
curl "http://localhost:7979/probe?module=default&target=http://host.docker.internal:8000/data.json" 

# HELP example_global_value Example of a top-level global value scrape in the json
# TYPE example_global_value untyped
example_global_value{environment="beta",location="planet-mars"} 1234
# HELP example_value_active Example of sub-level value scrapes from a json
# TYPE example_value_active untyped
example_value_active{environment="beta",id="id-A"} 1
example_value_active{environment="beta",id="id-C"} 1
# HELP example_value_boolean Example of sub-level value scrapes from a json
# TYPE example_value_boolean untyped
example_value_boolean{environment="beta",id="id-A"} 1
example_value_boolean{environment="beta",id="id-C"} 0
# HELP example_value_count Example of sub-level value scrapes from a json
# TYPE example_value_count untyped
example_value_count{environment="beta",id="id-A"} 1
example_value_count{environment="beta",id="id-C"} 3
```

add