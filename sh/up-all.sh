#!/bin/bash

# echo 'порождаем auth_proxy_network'
# docker network create auth_proxy_network

echo 'поднимаем'
docker compose -f docker-compose.yml up -d 
docker-compose -f docker-compose-front.yml up -d 

echo
echo "prometeus http://localhost:9090/graph"
echo "grafana   http://localhost:3000/"
echo "cadvisor  http://localhost:8080/containers/"